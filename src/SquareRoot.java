import java.util.Scanner;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class SquareRoot {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number;

        System.out.println("Enter number: ");
        number = sc.nextInt();

        if (number < 0) throw new IllegalArgumentException("Number cannot be negative!");

        System.out.println("Square root of number " + number  + "is " + Math.sqrt(number));
    }
}